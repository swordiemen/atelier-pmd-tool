package nl.utwente.pmdcreate.main;

public class Constants {
    public static final String TEMP_FILES_DIR = "./tmp";
    public static final String PDE_EXTENSION = ".pde";
    public static final String JAVA_EXTENSION = ".java";

}
